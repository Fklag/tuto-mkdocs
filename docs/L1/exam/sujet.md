# Parcours de tableaux

On rappelle qu'une matrice en Python est représentée par une liste de listes.  
L'élément $M[ i ][ j ]$ se trouve sur la ligne d'indice $i$ et sur la colonne d'indice $j$.

Compléter la fonction  `Parcours(M)` qui renvoie le nombre maximal de zero se trouvant sur une seule antidiagonale de la matrice carrée $M$.

!!! example "Exemples"
    `Parcours(M)` renvoi 3 pour $M = \begin{pmatrix} 1 & 0 & 3 & 1 & 0 \\ 2 & 4& 7 & 2 & \textbf{0} \\ 0 & 1 & 2 & \textbf{0} & 1 \\ 0 & 0 & \textbf{5} & 0 & 1 \\ 2 & \textbf{0} & 1 & 2 & 1\end{pmatrix}$  car il y a trois 0 sur l'antidiagonale $(0,0,5,0)$.  
    `Parcours(M)` renvoi 2 pour  
    $M = \begin{pmatrix} 0 & 2 & \textbf{0}\\ 4 & \textbf{3} &4\\ \textbf{0} & 6 & 4 \end{pmatrix}$



??? tip "Indice 1"
    On pourra s'inspirer des parcours de tableaux à deux dimensions

    ```python
    def parcours_antidiagonal(n):
        """Retourne la liste des indices (colonne,ligne) (!!attention ici
        ligne et colonne sont inversées!!)  des cases correspondant à un
        parcours de tableau de taille n x n en anti-diagonale.

        Ex: pour T = [[1,2,3],
                      [4,5,6],
                      [7,8,9]]
        le parcours correspond aux cases 9,6,8,3,5,7,2,4,1 et la 
        fonction retournera la liste d'indices [(2,2),(2,1),(1,2),(2,0) ...]

        """
        return []
    ```

??? tip "Indice 2"
    On pourra s'inspirer du code suivant

    ```python
    def parcours_antidiagonal(n):
        """Retourne la liste des indices (colonne,ligne) (!!attention ici
        ligne et colonne sont inversées!!)  des cases correspondant à un
        parcours de tableau de taille n x n en anti-diagonale.

        Ex: pour T = [[1,2,3],
                      [4,5,6],
                      [7,8,9]]
        le parcours correspond aux cases 9,6,8,3,5,7,2,4,1 et la 
        fonction retournera la liste d'indices [(2,2),(2,1),(1,2),(2,0) ...]

        """
        lcase =[]
        col,lig = n-1,n-1
        while col>=0:
            c,l = col, lig
            while c>=0 and l<n:
                lcase+=[(c,l)]
                c-=1
                l+=1
            if c<0:
                col-=1
            else:
                lig-=1
        return lcase
    ```

{{ IDE('exo1', MAX=1000) }}

# TRI INSERTION

Étant donnés deux tableaux d'entiers T1 et T2, on dira que le premier est plus grand que le second s'il contient plus de nombre pairs. Ainsi le tableau `[2,4,3,8]` et plus grand que le tableau `[1,2,3,5,15,35]`.

Soit $M$ un tableau de tableaux. Écrire une fonction `Tri(M)` qui tri le tableau de $M$, en fonction de la relation définie plus haut SUR LE MODÈLE DU TRI INSERTION. 

!!! example "Exemples"

    ```pycon
    >>> M = [[2,4,3,8],[0,0],[1,3,5,7]]
    >>> Tri(M)
    >>> print(M)  
      [[1,3,5,7], [0,0], [2,4,3,8]]
    ```

??? tip "Indice 1"
    On pourra s'inspirer des codes suivants où la fonction `tri_insertion(T)` renvoie de plus ici le nombre de comparaisons d'éléments du tableau qu'elle a effectuée.

    ```python
    def echanger(T,i,j):
        T[i],T[j]=T[j],T[i]

    def tri_insertion(T):
        nb_comp=0
        for i in range(1,len(T)):
            j=i
            while j>0 and T[j]<T[j-1]:
                echanger(T,j-1,j)
                nb_comp += 1
                j = j-1
            nb_comp += 1
        return nb_comp
    ```


{{ IDE('exo2', MAX=1000) }}

# Ranger

Soit $P=(x,y)$ un point du plan. On rappelle que la distance à l'origine de P est donnée par $dist(P) =  \sqrt{x^2+y^2}$.  
En Python on représente une liste de points par une liste de tuple.

Compléter la fonction  `Ranger(L)` qui range les points en fonction de leur distance à l'origine:

- les points à distance strictement inférieure à 1 au début
- les points à distance égale à 1 au milieu
- les points à distance strictement supérieure à 1 à la fin

L'algorithme utilisé devra de plus être **de complexité $\Theta(n)$** où $n$ est la taille de la liste.

Plusieurs rangements valides peuvent exister pour une même liste.

!!! example "Exemples"

    ```pycon
    >>> L = [ (0,1) , (-1,2), (1,1), (0.5, 0), ( -1,0), (0,0) ]
    >>> Ranger(L)
    >>> print(L)
    [(0,0), (0.5,0), (0,1), (-1,0), (1,1), (-1,2)]
    ```

??? tip "Indice 1"
    On pourra s'inspirer du [Problème du drapeau hollandais](https://fr.wikipedia.org/wiki/Probl%C3%A8me_du_drapeau_hollandais).  
    Le principe est le suivant. On part des deux extrémités, comme dans la procédure partition du tri rapide. On s'intéresse à la case courante du tableau T, dont on teste la couleur, et selon le résultat on procède à des échanges, de sorte qu'on ait à chaque étape à gauche une zone de bleus, puis une zone de blancs, puis une zone inconnue et enfin, à droite une zone de rouges. On va utiliser une variable b (blue), indice de la première case après la zone bleue connue; une variable w (white), indice de la première case après la zone blanche connue et une variable r (red), indice de la première case avant la zone rouge connue. L'algorithme élémentaire consiste à réduire la zone inconnue comprise entre les bornes w et r par test de la couleur de la case w.

    ```python
    def bbr(T):
        """ Retourne la liste des indices des cases à
        échanger pour obtenir un tableau à trois couleurs
        trié en bleu/blanc/rouge"""
        

    ```

??? tip "Indice 2"
    On pourra s'inspirer du code suivant

    ```python
    def bbr(T):
        """ Retourne la liste des indices des cases à
        échanger pour obtenir un tableau à trois couleurs
        trié en bleu/blanc/rouge"""
        b,w,r = 0,0,len(T)-1
        l_indice = []
        while w <= r:
            if T[w]=="blue":
                T[b],T[w] = T[w],T[b]
                l_indice += [(w,b)]
                b += 1
                w += 1
            elif T[w]=="white":
                w += 1
            else:
                T[w],T[r] = T[r],T[w]
                l_indice += [(w,r)]
                r -= 1
        return l_indice
    ```

{{ IDE('exo3', MAX=1000) }}

# Recherche dans un tableau trié

On considère une liste d'entiers triée dans l'ordre croissant contenant exactement 4 valeurs différentes.  
Compléter la fonction  `Recherche(L)` qui renvoie un tuple contenant ces 4 valeurs.

L'algorithme utilisé devra de plus être **de complexité $\Theta(\log(n))$** où $n$ est la taille de la liste.

!!! example "Exemples"

    ```pycon
    >>> L = [ 0,0,0,0,1,1,1,1,1,3,3,7,7]
    >>> Rechercher(L)
    (0,1,3,7)

    >>> L = [ -2,-2,10,10,10,10,10,10,10,10,10,10,25,25,25,25,25,70,70,70]
    >>> Rechercher(L)
    (-2,10,25,70)
    ```

??? tip "Indice 1"
    On pourra s'inspirer du code suivant où la fonction `recherche_binaire(T,x)` renvoie l'indice de l'élément x dans le tableau T s'il s'y trouve et -1 sinon, ainsi que le nombre de comparaisons effectuées.

    ```python
        def recherche_binaire(T,x):
        comp=0
        g,d = 0,len(T)-1
        while g <= d:
            m=(g+d)//2
            
            if T[m]==x:
                comp += 1
                return m,comp
            elif T[m]>x:
                comp += 2
                d = m-1
            else:
                comp += 2
                g = m+1
        return -1, comp
    ```

{{ IDE('exo4', MAX=1000) }}


# Manipulation élémentaire des piles/files

On considère les chaînes de caractères formées à partir des chiffres 0,1,2,3.

Écrire une fonction `chaines_0123(n)` qui renvoie, **dans l'ordre lexicographique**, la liste de toutes les chaînes de longueur $n$ pour lesquelles ils n'y a jamais deux caractères consécutifs dont la somme est un multiple de 4.  
On pourra utiliser le module `pile_file.py`.

!!! example "Exemples"

    ```pycon
    >>> chaines_0123(2)
    ['01', '02', '03', '10', '11', '12', '20', '21', '23', '30', '32', '33']
    ```

Voici un module contenant une classe de liste chainée `ListeC`, possédant, en autres, les méthodes `#!python .est_vide()` , `#!python .ajouter_debut(m)` ,  `#!python ajouter_fin(m)` , `#!python supprimer_debut()`, $\ldots$

??? "Module pilefile.py"
    ```py3
    --8<--- "docs/L1/exam/pilefile.py"
    ```

On peut utiliser les classes de ce module dans un autre programme en utilisant l'instruction :  
            `#!python from pilefile import *`  

??? tip "Indice 1"
    On pourra s'inspirer du code suivant où la fonction `affiche_ternaire(n)` affiche toutes les chaı̂nes ternaires de longueurs n.  

    ```python
    from pilefile import *

    def affiche_ternaire(n):
        """Affiche tous les chaines ternaires de longueur n.

        """
        F = file_init()
        enfiler(F,"")
        deb = (3**n-1)//2-1 #(3**n-3)//2
        for i in range(0,deb,3):
            x = defiler(F)
            for c in '012':
                enfiler(F,x+c)

        for i in range(len(F)):
            x = defiler(F)
            for c in '012':
                print(x+c)
    ```

    Par exemple:  

    ```pycon
    >>> affiche_ternaire(2)
    00
    01
    02
    10
    11
    12
    20
    21
    22
    ```

{{ IDE('exo5', MAX=1000) }}