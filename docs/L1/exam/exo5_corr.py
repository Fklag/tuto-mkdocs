def file_init():
    """Retourne une file vide"""
    return []

def file_vide(F):
    """Retourne True si la file <F> est vide, False sinon"""
    return F == []

def enfiler(F,x):
    """Ajoute l'element <x> a la queue de la file <F>"""
    F.append(x)
    for i in range(len(F)-1,0,-1):
        F[i],F[i-1] = F[i-1],F[i]

def defiler(F):
    """Defile l'element en tete de la File <F> et le retourne"""
    return F.pop()

def chaines_0123(n):
    f = file_init()
    res = []
    for car in '0123':
        enfiler(f,car)
    while file_vide(f) == False:
        x = defiler(f)
        if len(x)==n:
            res.append(x)
        else:
            for car in '0123':
                if (int(car) + int(x[-1])) % 4 != 0:
                    enfiler(f, x+car)
    return res

# tests
assert chaines_0123(2) == ['01', '02', '03', '10', '11', '12', '20', '21', '23', '30', '32', '33']


