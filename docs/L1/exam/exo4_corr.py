def Rechercher(L):
    val = []
    i=0
    while (i<len(L)):
        if L[i] not in val:
            val.append(L[i])
        i+=1
    return tuple(val)


def recherche_binaire(T):
    x1 = T[0]-0.5
    x2 = T[-1]+0.5
    val = []
    g,d = 0,len(T)-1
    while g <= d:
        m=(g+d)//2
        if T[m]==x1:
            return m
        elif T[m]>x1:
            if T[m] not in val :
                val.append(T[m])
            d = m-1
        else:
            if T[m] not in val :
                val.append(T[m])
            g = m+1
    g,d = 0,len(T)-1
    while g <= d:
        m=(g+d)//2
        if T[m]==x2:
            return m
        elif T[m]>x2:
            if T[m] not in val :
                val.append(T[m])
            d = m-1
        else:
            if T[m] not in val :
                val.append(T[m])
            g = m+1

    return tuple(val)

# tests

L = [ 0,0,0,0,1,1,1,1,1,3,3,7,7]
assert Rechercher(L) == (0,1,3,7)

L = [-2,-2,10,10,10,10,10,10,10,10,10,10,25,25,25,25,25,70,70,70]
assert Rechercher(L) == (-2,10,25,70)


