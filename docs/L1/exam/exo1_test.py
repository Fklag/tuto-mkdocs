#from random import *

# ici je code la fonction que les etudiants ont a realiser
# def __answer__(M):
#     """Retourne le nombre maximal de zero se trouvant sur une antidiagonale"""
#     nb_zero = 0
#     max_zero = 0
#     for lig in range(len(M)):
#         nb_zero = 0
#         l,c = lig, len(M)-1
#         while l<len(M) and c>=0:
#             if M[l][c] == 0:
#                 nb_zero += 1
#             l+=1
#             c-=1
#         if nb_zero > max_zero:
#             max_zero = nb_zero
#     for col in range(0,len(M)-1):
#         nb_zero = 0
#         l,c = 0, col
#         while l<len(M) and c>=0:
#             if M[l][c] == 0:
#                 nb_zero += 1
#             l+= 1
#             c-=1
#         if nb_zero > max_zero:
#             max_zero = nb_zero
#
#     return max_zero

# les tests avec tirages aleatoires
# Ntest=7
# for ntest in range(2,Ntest+2):
#     print("Test :",ntest)
#     M = [ [ randint(0,10)%2 for i in range(ntest)] for j in range(ntest)]
#     print(M)
#     print(__answer__(M))

assert Parcours([[1, 1], [1, 1]]) == 0
assert Parcours([[0, 1, 1], [0, 0, 1], [0, 1, 0]]) == 2
assert Parcours([[0, 0, 1, 0], [1, 1, 0, 0], [0, 1, 1, 0], [1, 0, 0, 1]]) == 2
assert Parcours([[1, 0, 1, 0, 1], [0, 0, 0, 1, 1], [1, 1, 0, 1, 1], [0, 0, 0, 1, 0], [0, 1, 1, 0, 1]]) == 3
assert Parcours([[0, 0, 0, 1, 1, 0], [0, 1, 0, 0, 0, 0], [0, 1, 1, 1, 1, 0], [0, 0, 1, 0, 1, 1], [0, 1, 0, 0, 0, 0], [0, 0, 0, 0, 0, 1]]) == 4
assert Parcours([[0, 0, 0, 0, 0, 1, 0], [0, 0, 1, 0, 1, 1, 0], [0, 0, 0, 0, 1, 0, 0], [1, 0, 1, 1, 0, 0, 1], [1, 1, 1, 1, 0, 0, 0], [1, 0, 0, 0, 1, 1, 1], [0, 1, 1, 0, 0, 1, 0]]) == 4
assert Parcours([[0, 1, 0, 0, 0, 1, 1, 0], [0, 0, 0, 1, 1, 0, 1, 1], [1, 1, 0, 1, 0, 1, 1, 1], [0, 0, 1, 0, 0, 0, 1, 0], [0, 1, 1, 1, 0, 0, 1, 0], [0, 1, 1, 0, 0, 0, 0, 1], [0, 0, 0, 1, 1, 1, 1, 0], [0, 1, 0, 0, 0, 0, 0, 0]]) == 4



