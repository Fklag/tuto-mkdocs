def echanger(T,i,j):
    T[i],T[j]=T[j],T[i]

def __count(T):
    cpt=0
    for x in T:
        if x%2==0:
            cpt+=1
    return cpt

def sel(T,i):
    imin = i
    for j in range(i+1,len(T)):
        if __count(T[imin])>__count(T[j]):
            imin = j
    return imin

def Tri(T):
    for i in range(len(T)):
        imin = sel(T,i)
        echanger(T,imin,i)


M = [[2,4,3,8],[0,0],[1,3,5,7]]
Tri(M)
assert M == [[1,3,5,7], [0,0], [2,4,3,8]]

