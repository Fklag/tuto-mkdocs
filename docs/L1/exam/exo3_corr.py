def __check__(M):
    """verifie que L est range correctement"""
    i = 0
    while (i<len(L)) and L[i][0]**2+L[i][1]**2 < 1:
        i+=1
    while (i<len(L)) and L[i][0]**2+L[i][1]**2  == 1:
        i+=1
    while (i<len(L)) and L[i][0]**2+L[i][1]**2  > 1:
        i+=1
    return i == len(L)


def Ranger(L):
    b,w,r = 0,0,len(L)-1
    while w <= r:
        if L[w][0]**2+L[w][1]**2 < 1:
            L[b],L[w] = L[w],L[b]
            b += 1
            w += 1
        elif L[w][0]**2+L[w][1]**2 == 1:
            w += 1
        else:
            L[w],L[r] = L[r],L[w]
            r -= 1

L = [ (0,1) , (-1,2), (1,1), (0.5, 0), ( -1,0), (0,0) ]
Ranger(L)
# #L == [(0,0), (0.5,0), (0,1), (-1,0), (1,1), (-1,2)]
#L == [(0, 0), (0.5, 0), (-1, 0), (0, 1), (1, 1), (-1, 2)]

assert __check__(L)