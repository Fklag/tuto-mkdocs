from random import *

# ici je code la fonction de vérification
def __answer__(L):
    val = []
    i=0
    while (i<len(L)):
        if L[i] not in val:
            val.append(L[i])
        i+=1
    return tuple(val)

def Rechercher(L):
    val = []
    i=0
    while (i<len(L)):
        if L[i] not in val:
            val.append(L[i])
        i+=1
    return tuple(val)

# les tests avec tirages aleatoires
Ntest=10
for ntest in range(4,Ntest+4):
    val = [randrange(-10,0),randrange(0,5),randrange(5,10),randrange(10,20),]
    L = []
    for v in val:
        L+=[v for x in range(randrange(1,20))]

    assert Rechercher(L) == __answer__(L)

