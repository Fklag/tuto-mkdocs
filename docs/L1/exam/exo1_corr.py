def Parcours(M):
    """Retourne le nombre maximal de zero se trouvant sur une antidiagonale"""
    nb_zero = 0
    max_zero = 0
    for lig in range(len(M)):
        nb_zero = 0
        l,c = lig, len(M)-1
        while l<len(M) and c>=0:
            if M[l][c] == 0:
                nb_zero += 1
            l+=1
            c-=1
        if nb_zero > max_zero:
            max_zero = nb_zero
    for col in range(0,len(M)-1):
        nb_zero = 0
        l,c = 0, col
        while l<len(M) and c>=0:
            if M[l][c] == 0:
                nb_zero += 1
            l+= 1
            c-=1
        if nb_zero > max_zero:
            max_zero = nb_zero

    return max_zero

